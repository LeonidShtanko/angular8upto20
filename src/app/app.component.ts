import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Initial'

  toggle: any = false

  onInput(event: any) {
    this.title = event.target.value
  }

  arr = [1, 2, 3, 4, 5, 8, 13]

  objs = [
    {
      title: 'Post 1', author: 'Leo', comments: [
        { name: 'Max', text: 'lorem 1' },
        { name: 'Max', text: 'lorem 2' },
        { name: 'Max', text: 'lorem 3' }
      ]
    },
    {
      title: 'Post 2', author: 'Leo 2', comments: [
        { name: 'Max 2', text: 'lorem 1' },
        { name: 'Max 2', text: 'lorem 2' },
        { name: 'Max 2', text: 'lorem 3' }
      ]
    },
  ]

  now = new Date()



}
